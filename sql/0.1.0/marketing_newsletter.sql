CREATE TABLE `marketing_newsletter` (
    `ID` varchar(20) NOT NULL,
    `email` varchar(255) NOT NULL,
    `state` varchar(25) DEFAULT NULL,
    `token` varchar(20) DEFAULT NULL,
    `subscribing_time` datetime DEFAULT NULL,
    `subscribing_ip` text,
    `unsubscribe_time` datetime DEFAULT NULL,
    `optin_ip` text,
    `optin_time` datetime DEFAULT NULL,
    `state_log` json DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `marketing_newsletter__ID` (`ID`),
    KEY `marketing_newsletter__email` (`email`),
    KEY `marketing_newsletter__state` (`state`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
