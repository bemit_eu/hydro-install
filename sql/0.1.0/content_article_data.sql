CREATE TABLE `content_article_data` (
    `ID` varchar(20) NOT NULL,
    `article` varchar(20) NOT NULL,
    `locale` varchar(8) NOT NULL,
    `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
    ON UPDATE CURRENT_TIMESTAMP,
    `page_type` varchar(20) DEFAULT NULL,
    `meta` json DEFAULT NULL,
    `main_text` mediumtext,
    `doc_tree` json DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `content_article_data__ID` (`ID`),
    KEY `content_article_data__article` (`article`),
    KEY `content_article_data__locale` (`locale`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
