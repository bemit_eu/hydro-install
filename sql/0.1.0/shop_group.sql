CREATE TABLE `shop_group` (
    `ID` varchar(20) NOT NULL,
    `name` varchar(150) NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `shop_group__ID` (`ID`),
    KEY `shop_group__name` (`name`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
