CREATE TABLE `content_section` (
    `ID` varchar(20) NOT NULL,
    `name` varchar(150) NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `content_section__ID` (`ID`),
    KEY `content_section__name` (`name`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
