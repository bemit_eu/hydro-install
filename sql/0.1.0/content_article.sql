CREATE TABLE `content_article` (
    `ID` varchar(20) NOT NULL,
    `section` varchar(20) NOT NULL,
    `tag` varchar(20) DEFAULT NULL,
    `name` varchar(150) NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `content_article__ID` (`ID`),
    KEY `content_article__section` (`section`),
    UNIQUE KEY `content_article__tag` (`tag`),
    KEY `content_article__name` (`name`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
