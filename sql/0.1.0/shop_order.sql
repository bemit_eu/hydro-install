CREATE TABLE `shop_order` (
    `ID` varchar(20) NOT NULL,
    `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `locale` varchar(20) DEFAULT NULL,
    `state` varchar(20) DEFAULT NULL,
    `state_log` json DEFAULT NULL,
    `customer` json DEFAULT NULL,
    `products` json DEFAULT NULL,
    `shipping` json DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `shop_order__ID` (`ID`),
    KEY `shop_order__date` (`date`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
