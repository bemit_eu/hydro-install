CREATE TABLE `user_base` (
    `ID` varchar(20) NOT NULL,
    `name` varchar(150) NOT NULL,
    `pass` varchar(200) DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `user_base__ID` (`ID`),
    UNIQUE KEY `user_base__name` (`name`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
