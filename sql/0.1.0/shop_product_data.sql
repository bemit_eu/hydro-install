CREATE TABLE `shop_product_data` (
    `ID` varchar(20) NOT NULL,
    `product` varchar(20) NOT NULL,
    `locale` varchar(8) NOT NULL,
    `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `date_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
    ON UPDATE CURRENT_TIMESTAMP,
    `product_no` varchar(20) DEFAULT NULL,
    `active` bool DEFAULT FALSE,
    `hidden` bool DEFAULT FALSE,
    `buy_able` bool DEFAULT FALSE,
    `tax_group` varchar(20) DEFAULT NULL,
    `price` int,
    `data` json DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `shop_product_data__ID` (`ID`),
    KEY `shop_product_data__article` (`product`),
    KEY `shop_product_data__active` (`active`),
    KEY `shop_product_data__locale` (`locale`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;