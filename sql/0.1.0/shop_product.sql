CREATE TABLE `shop_product` (
    `ID` varchar(20) NOT NULL,
    `group` varchar(20) NOT NULL,
    `product_type` varchar(20) DEFAULT NULL,
    `name` varchar(150) NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `shop_product__ID` (`ID`),
    KEY `shop_product__name` (`name`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
