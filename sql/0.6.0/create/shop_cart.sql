CREATE TABLE `shop_cart`
(
    `ID`          varchar(20)  NOT NULL,
    `user`        varchar(255) NOT NULL,
    `contents`    json                  DEFAULT NULL,
    `date_create` timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `date_update` timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP
        ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `shop_cart__ID` (`ID`),
    KEY `shop_cart__user` (`user`),
    KEY `shop_cart__create` (`date_create`),
    KEY `shop_cart__update` (`date_update`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
