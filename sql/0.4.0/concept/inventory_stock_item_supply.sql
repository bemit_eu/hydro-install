CREATE TABLE `inventory_stock_item_supply`
(
    `ID`               varchar(20) NOT NULL,
    `date`             timestamp   NOT NULL,
    `delivery_planned` timestamp   NOT NULL,
    `delivery_actual`  timestamp   NOT NULL,
    `amount_planned`   timestamp   NOT NULL,
    `amount_received`  timestamp   NOT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `inventory_stock_item_supply__ID` (`ID`),
    KEY `inventory_stock_item_supply__date` (`date`),
    KEY `inventory_stock_item_supply__delivery_planned` (`delivery_planned`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
