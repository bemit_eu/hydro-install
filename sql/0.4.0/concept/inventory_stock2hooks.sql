CREATE TABLE `inventory_stock2hooks`
(
    `stock`   varchar(20)  NOT NULL,
    `hook` varchar(150) NOT NULL,
    PRIMARY KEY (`stock`, `hook`),
    UNIQUE KEY `inventory_stock2hooks__ID` (`stock`, `hook`),
    KEY `inventory_stock2hooks__stock` (`stock`),
    KEY `inventory_stock2hooks__hook` (`hook`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
