CREATE TABLE `inventory_stock_item_history`
(
    `stock_item`       varchar(20) NOT NULL,
    `datetime`         DATETIME,
    `amount_available` bigint,
    `amount_reserve`   bigint,
    KEY `inventory_stock_item_history__stock_item` (`stock_item`),
    KEY `inventory_stock_item_history__datetime` (`datetime`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
