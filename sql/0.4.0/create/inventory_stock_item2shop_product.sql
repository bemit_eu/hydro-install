CREATE TABLE `inventory_stock_item2shop_product`
(
    `stock_item`   varchar(20)  NOT NULL,
    `shop_product` varchar(150) NOT NULL,
    PRIMARY KEY (`stock_item`, `shop_product`),
    UNIQUE KEY `inventory_stock_item2shop_product__ID` (`stock_item`, `shop_product`),
    KEY `inventory_stock_item2shop_product__stock_item` (`stock_item`),
    KEY `inventory_stock_item2shop_product__shop_product` (`shop_product`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
