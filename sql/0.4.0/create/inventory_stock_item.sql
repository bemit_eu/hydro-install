CREATE TABLE `inventory_stock_item`
(
    `ID`               varchar(20)  NOT NULL,
    `stock`            varchar(20)  NOT NULL,
    `sku`              varchar(150) NOT NULL,
    `name`             varchar(150) DEFAULT NULL,
    `location`         json         DEFAULT NULL,
    `amount_reserved`  bigint       DEFAULT 0,
    `amount_available` bigint       DEFAULT 0,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `inventory_stock_item__ID` (`ID`),
    KEY `inventory_stock_item__stock` (`stock`),
    KEY `inventory_stock_item__sku` (`sku`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
