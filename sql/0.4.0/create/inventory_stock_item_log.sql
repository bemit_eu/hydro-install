CREATE TABLE `inventory_stock_item_log`
(
    `ID`               int auto_increment,
    `datetime`         DATETIME    default now(),
    `stock_item`       varchar(20) NOT NULL,
    `order`            varchar(20) DEFAULT NULL,
    `amount_available` bigint,
    `amount_reserved`  bigint,
    PRIMARY KEY (`ID`),
    KEY `inventory_stock_item_log__stock_item` (`stock_item`),
    KEY `inventory_stock_item_log__datetime` (`datetime`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
