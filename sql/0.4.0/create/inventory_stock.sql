CREATE TABLE `inventory_stock`
(
    `ID`         varchar(20) NOT NULL,
    `warehouse`  varchar(20) NOT NULL,
    `name`       varchar(150) NOT NULL,
    `sku_prefix` varchar(150) DEFAULT NULL,
    `desc`       text         DEFAULT NULL,
    `location`   json         DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE KEY `inventory_stock__ID` (`ID`),
    KEY `inventory_stock__warehouse` (`warehouse`),
    KEY `inventory_stock__name` (`name`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4;
