ALTER TABLE shop_product_data
    ADD variation_of varchar(20) DEFAULT NULL;

ALTER TABLE shop_product_data
    ADD bundle boolean DEFAULT FALSE;