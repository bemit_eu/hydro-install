create table message
(
    ID           varchar(20)                         not null,
    time         timestamp default CURRENT_TIMESTAMP not null,
    recipient_id varchar(20)                         null,
    recipient    json                                null,
    sender_id    varchar(20)                         null,
    sender       json                                null,
    subject      text                                null,
    text         text                                null,
    type         varchar(100)                        null,
    hook         varchar(50)                         null,
    constraint ID
        unique (ID)
);

create index message__time
    on message (time);

create index message__recipient
    on message (recipient_id);

create index message__sender
    on message (sender_id);

create index message__type
    on message (type);

create index message__hook
    on message (hook);

alter table message
    add primary key (ID);