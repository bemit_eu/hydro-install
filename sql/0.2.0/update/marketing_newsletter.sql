ALTER TABLE marketing_newsletter
    ADD hook varchar(50) DEFAULT NULL;
ALTER TABLE marketing_newsletter
    ADD source varchar(50) DEFAULT NULL;
ALTER TABLE marketing_newsletter
    ADD optin_reference varchar(150) DEFAULT NULL;