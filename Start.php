<?php

namespace HydroInstall;

use Hydro\Console\Command;

class Start extends Command {
    protected static $defaultName = 'install:start';

    protected function configure() {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new user.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create a user...');

    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        // ...
    }
}