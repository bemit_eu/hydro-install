<?php

namespace HydroInstall;

use Flood\Captn;
use Flood\Component\Route\Hook\HookFile;
use Flood\Component\Route\Hook\HookFileI;
use Hydro\Container;
use Hydro\Console;

class HookHydroInstall extends HookFile implements HookFileI {
    static $hook_for_api;

    public function __construct() {
        Captn\EventDispatcher::on(
            'hydro.exec.addCliCommands',
            [$this, 'addCliCommands']
        );
    }

    /**
     * @param Console $cli
     */
    public function addCliCommands($cli) {
        error_log('addclicommands');
        $cli->add(new Start());
    }
}